# Inherit device configuration for t40s.
$(call inherit-product, device/nvidia/t40s/full_t40s.mk)

# Inherit some common lineage stuff.
ifeq ($(ALTERNATE_BUILD),true)
$(call inherit-product, vendor/cm/config/common_full_tv.mk)
else
$(call inherit-product, vendor/cm/config/common_full_phone.mk)
endif

PRODUCT_NAME := lineage_t40s
PRODUCT_DEVICE := t40s
